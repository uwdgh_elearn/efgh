<?php
/**
 * Table row output for a download
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if( ! $dlm_download ) {
	return;
}

/** @var DLM_Download $dlm_download */
?>
<tr role="link" data-href="<?php $dlm_download->the_download_link(); ?>" title="<?php $dlm_download->the_title(); ?>">
	<td><span class="filetype-icon <?php echo 'filetype-' . $dlm_download->get_version()->get_filetype(); ?>" title="<?php echo $dlm_download->get_version()->get_filetype(); ?>"></span></td>
	<td><?php $dlm_download->the_title(); ?></td>
	<td><code><?php echo $dlm_download->get_version()->get_filename(); ?></code> <?php if ( $dlm_download->get_version()->has_version_number() ) {
		 printf( __( ' <i>(Version %s)</i>', 'download-monitor' ), $dlm_download->get_version()->get_version_number() );
	 } ?></td>
 	<td><span style="white-space: nowrap; color: #9E9E9E;"><time datetime="<?php echo $dlm_download->get_version()->get_date()->format( 'Y-m-d H:i:s' ); ?>"><?php echo $dlm_download->get_version()->get_date()->format( get_option( 'date_format' ) ); ?></time></span></td>
	<td><span style="white-space: nowrap;"><?php echo $dlm_download->get_version()->get_filesize_formatted(); ?></span></td>
	<td><div class="uw-cloud-icon"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/png/uw_icons_cloud_download-e1469485632674.png'; ?>"></div>
 	</td>
</tr>
