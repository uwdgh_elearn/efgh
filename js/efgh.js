// The document ready event executes when the HTML-Document is loaded
// and the DOM is ready.
jQuery(document).ready(function( $ ) {

  // set custom download event on touchstart
  $(document).on('touchstart', 'table.table--dlm tbody tr[role=link]', function(){
    download = true;
  })
  // unset download event on touchmove
  $(document).on('touchmove', 'table.table--dlm tbody tr[role=link]', function(){
    download = false;
  })
  $(document).on('click touchend', 'table.table--dlm tbody tr[role=link]', function(e){
    if (e.type == "click") download = true; // set download event on click
    if (download){
      window.location = $(this).data('href'); // the download
      return false;
    }
  });

})

// The window load event executes after the document ready event,
// when the complete page is fully loaded.
jQuery(window).load(function () {

})
