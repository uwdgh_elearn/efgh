<?php
  if ( !is_user_logged_in() && !is_efgh_session() ) {
    do_action('efgh_end_session');
    auth_redirect();
  }
?>

<?php get_header(); ?>

<?php get_template_part( 'header', 'image' ); ?>

<div class="container uw-body">

  <div class="row">

    <div <?php uw_content_class(); ?> role='main'>

      <?php uw_site_title(); ?>

      <?php get_template_part( 'menu', 'mobile' ); ?>

      <?php get_template_part( 'breadcrumbs' ); ?>

      <div id='main_content' class="uw-body-copy" tabindex="-1">

      <?php $taxonomy = get_taxonomy( 'dlm_download_category' ); ?>

      <h1><?php echo $taxonomy->labels->singular_name ?>: <?php echo single_cat_title( '', false ); ?></h1><hr>

      <?php

      if ( get_query_var("dlm_download_category") ) {

          echo do_shortcode( '[su_posts template="partials/su_dlm-table.php" taxonomy="category" tax_term="'.get_query_var("dlm_download_category").'"]' );

      }

      ?>

      </div>

    </div>

    <?php get_sidebar() ?>
    <?php echo do_shortcode( '[efgh_tax_terms taxonomy="dlm_download_category"]' ); ?>
    <?php echo do_shortcode( '[efgh_tax_terms taxonomy="dlm_download_tag"]' ); ?>

  </div>

</div>

<?php get_footer(); ?>
