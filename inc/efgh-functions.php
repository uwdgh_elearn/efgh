<?php
/* start session */
function efgh_session_start() {
  if( ( !session_id() ) || ( session_status() !== 2 ) || ( session_status() !== PHP_SESSION_ACTIVE ) ) {
    session_start();
    efgh_set_remote_user();
  }
}
add_action('init', 'efgh_session_start');
add_action('wp_login','efgh_session_start');

/* set custom session variable for the remote user */
function efgh_set_remote_user() {
  if ( !array_key_exists('uwNetID', $_SESSION) && array_key_exists('REMOTE_USER', $_SERVER) ) {
    $_SESSION['uwNetID']=$_SERVER['REMOTE_USER'];
  }
}

/* destroy session */
function efgh_session_destroy() {
  unset($_SESSION["uwNetID"]);
  session_destroy();
}
add_action('wp_logout','efgh_session_destroy');
//add_action('wp_login','efgh_session_destroy');
/* custom hook */
add_action('efgh_end_session', 'efgh_session_destroy');
// place the following do_action hook anywhere in the application you want to end the session.
//do_action('efgh_end_session');

/* Is the current authenticated user part of the EFGH list of users */
function is_efgh_session() {
  if ( ( !is_null($_SESSION['uwNetID']) ) && (efgh_netids()) && ( in_array($_SESSION['uwNetID'], efgh_netids(), true) ) ) {
    return true;
  }
  return false;
}

/* Array of NetIDs. Resides in public_html/efgh_netids.txt file on server.
  Needs to be the same list of NetIDs as the shib-users in public_html/.htaccess */
function efgh_netids() {
  return file(home_url() . '/efgh_netids.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}

/* Add theme styles and scripts */
function efgh_scripts_and_styles() {

    wp_register_script('efgh', get_stylesheet_directory_uri() . '/js/efgh.js', array('jquery'));
    wp_enqueue_script('efgh');
    //wp_register_script('efgh-sortable', get_stylesheet_directory_uri() . '/js/sortable.js', array('jquery'));
    //wp_enqueue_script('efgh-sortable');
}
add_action( 'wp_enqueue_scripts', 'efgh_scripts_and_styles');

/* get taxonomy term terms shortcode
*  attributes:
* * taxonomy [optional], default value is 'category'
*/
function efgh_tax_terms( $atts = [] ) {
  $taxonomy = get_taxonomy( 'category' );
  if ( $atts['taxonomy'] ) {
    $taxonomy = get_taxonomy( $atts['taxonomy'] );
  }
  if ($taxonomy) {
    $output = '<h2>' . $taxonomy->label . '</h2>';
    $terms = get_terms( $taxonomy->name );
    foreach ( $terms as $term ) {
      $output .='<span class="uwdgh-tag"><a rel="tag" href="' . get_term_link($term->slug, $taxonomy->name) . '">' . $term->name . '</a></span> ';
    }
  } else {
    $output = '<div style="color: red; font-weight: bold;">Taxonomy <i>' . $atts['taxonomy'] . '</i> not found!</div>';
  }
  return $output;
}
// Add shortcode
add_shortcode('efgh_tax_terms', 'efgh_tax_terms');
// Enable shortcode execution inside text widgets
add_filter('widget_text', 'do_shortcode');
