<?php
/**
* Theme Class
*/

if ( !class_exists( 'UWDGH' ) ) {

  include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
  include_once( get_stylesheet_directory() . '/inc/override-functions.php' );
  include_once( get_stylesheet_directory() . '/inc/UWDGH_Template_Slideshow.class.php' );
  include_once( get_stylesheet_directory() . '/inc/UWDGH_Template_Video.class.php' );

  class UWDGH {

    /**
    * class initializaton
    */
    public static function init() {
      // register thinstrip menu
      add_action( 'init', array( __CLASS__, 'uwdgh_register_thinstrip_menu' ));
      // add menu item
      add_action('admin_menu', array( __CLASS__, 'uwdgh_admin_menu_item' ));
      // register settings
      register_setting("uwdgh_theme_options", "uwdgh_theme_show_post_tax_terms", array('default' => 1,) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_scroll_to_top", array('default' => 1,) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_override_quicklinks", array('default' => 0,) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_hide_front_page_title", array('default' => 0,) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_show_watermark", array('default' => 0,) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_show_watermark_text", array('default' => 'dev',) );
      register_setting("uwdgh_theme_options", "uwdgh_theme_show_php_inspector", array('default' => 0,) );
      //add parent style
      add_action( 'wp_enqueue_scripts', array( __CLASS__, 'uwdgh_scripts_and_styles' ));
      // add js and css for options
      add_action( 'wp_print_scripts', array( __CLASS__, 'uwdgh_enqueue_theme_options_scripts' ), 100 );
      add_action( 'wp_enqueue_scripts', array( __CLASS__, 'uwdgh_enqueue_theme_options_styles' ), 12 );
      // register theme headers
      register_default_headers( self::uwdgh_default_headers() );
    }

    /**
    * Add options page menu item to Appearance menu
    */
    static function uwdgh_admin_menu_item() {
        add_submenu_page('themes.php', 'UW DGH | Theme options', 'UW DGH | Theme options', 'manage_options', 'uwdgh-theme-options', array( __CLASS__, 'uwdgh_theme_options_page' ));
    }

    /**
    * Options page
    */
    static function uwdgh_theme_options_page() { ?>
    <div class="wrap">
        <h2>UW DGH | <?php _e('Theme Options','uwdgh');?></h2>
        <p><?php _e('Here you can manage options for the child-theme.','uwdgh');?></p>
        <form action="options.php" method="post" id="uwdgh-theme-options-form">
            <?php settings_fields('uwdgh_theme_options'); ?>
            <table class="form-table">
                <tr class="even" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_hide_front_page_title">
                            <?php _e('Hide the title on the front page for the Default Template','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_hide_front_page_title" name="uwdgh_theme_hide_front_page_title"  value="1" <?php checked(1, get_option('uwdgh_theme_hide_front_page_title'), true); ?> />
                        <span><em>(Default: unchecked)</em></span>
                    </td>
                </tr>
                <tr class="odd" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_show_post_tax_terms">
                            <?php _e('Show taxonomy terms from Categories and Tags in posts','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_show_post_tax_terms" name="uwdgh_theme_show_post_tax_terms"  value="1" <?php checked(1, get_option('uwdgh_theme_show_post_tax_terms'), true); ?> />
                        <span><em>(Default: checked)</em></span>
                    </td>
                </tr>
                <tr class="even" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_scroll_to_top">
                            <?php _e('Enable Back-to-Top link','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_scroll_to_top" name="uwdgh_theme_scroll_to_top"  value="1" <?php checked(1, get_option('uwdgh_theme_scroll_to_top'), true); ?> />
                        <span><em>(Default: checked)</em></span>
                    </td>
                </tr>
                <tr class="even" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_override_quicklinks">
                            <?php _e('Override quicklinks styling','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_override_quicklinks" name="uwdgh_theme_override_quicklinks"  value="1" <?php checked(1, get_option('uwdgh_theme_override_quicklinks'), true); ?> />
                        <span><em>(Default: unchecked)</em></span><br>
                        <span><?php _e('The UW-2014 theme has styling erroneously hardcoded for the 6th and 7th items in the quicklinks menu (Facebook and Twitter icons).','uwdgh');?></span><br>
                        <span><?php _e('This option overrides the quicklinks css and creates uniform menu items for all links in a custom menu added to the Quick Links display location.','uwdgh');?></span>
                    </td>
                </tr>
                <tr class="odd" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_show_watermark">
                            <?php _e('Show the "development" banner','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_show_watermark" name="uwdgh_theme_show_watermark"  value="1" <?php checked(1, get_option('uwdgh_theme_show_watermark'), true); ?> />
                        <span><em>(Default: unchecked)</em></span><br><br>
                        <span><?php _e('Banner text','uwdgh');?>:</span>
                        <select id="uwdgh_theme_show_watermark_text" name="uwdgh_theme_show_watermark_text">
                          <option value="demo" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'demo' ); ?>>demo</option>
                          <option value="dev" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'dev' ); ?>>dev</option>
                          <option value="draft" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'draft' ); ?>>draft</option>
                          <option value="sandbox" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'sandbox' ); ?>>sandbox</option>
                          <option value="staging" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'staging' ); ?>>staging</option>
                          <option value="test" <?php selected( get_option('uwdgh_theme_show_watermark_text'), 'test' ); ?>>test</option>
                        </select>
                    </td>
                </tr>
                <tr class="odd" valign="top">
                    <th scope="row">
                        <label for="uwdgh_theme_show_php_inspector">
                            <?php _e('Show the PHP INSPECTOR','uwdgh');?>
                        </label>
                    </th>
                    <td>
                        <input type="checkbox" id="uwdgh_theme_show_php_inspector" name="uwdgh_theme_show_php_inspector"  value="1" <?php checked(1, get_option('uwdgh_theme_show_php_inspector'), true); ?> />
                        <span><em>(Default: unchecked)</em></span><br>
                        <span><?php _e('Only use this for debugging purposes.','uwdgh');?></span><br>
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php }

    /**
     * add js for options
     */
    static function uwdgh_enqueue_theme_options_scripts() {
        //
      if ( get_option('uwdgh_theme_scroll_to_top') && file_exists(get_stylesheet_directory() . '/assets/admin/options/scroll-up/') ) {
          wp_register_script('uwdgh-theme-scroll-up', get_stylesheet_directory_uri() . '/assets/admin/options/scroll-up/scroll-up.js', array('jquery'));
          wp_enqueue_script('uwdgh-theme-scroll-up');
      }
      if ( get_option('uwdgh_theme_show_watermark') && file_exists(get_stylesheet_directory() . '/assets/admin/options/watermark/') ) {
          wp_register_script('uwdgh-theme-show-watermark', get_stylesheet_directory_uri() . '/assets/admin/options/watermark/watermark.js', array('jquery'));
          wp_enqueue_script('uwdgh-theme-show-watermark');
      }
    }

    /**
     * add css for options
     */
    static function uwdgh_enqueue_theme_options_styles() {
      if ( get_option('uwdgh_theme_scroll_to_top') && file_exists(get_stylesheet_directory() . '/assets/admin/options/scroll-up/') ) {
        wp_enqueue_style( 'uwdgh-theme-scroll-up', get_stylesheet_directory_uri() . '/assets/admin/options/scroll-up/scroll-up.css' );
      }
      if ( get_option('uwdgh_theme_override_quicklinks') && file_exists(get_stylesheet_directory() . '/assets/admin/options/override-quicklinks/') ) {
        wp_enqueue_style( 'uwdgh-theme-override-quicklinks', get_stylesheet_directory_uri() . '/assets/admin/options/override-quicklinks/override-quicklinks.css' );
      }
      if ( get_option('uwdgh_theme_show_watermark') && file_exists(get_stylesheet_directory() . '/assets/admin/options/watermark/') ) {
        wp_enqueue_style( 'uwdgh-theme-show-watermark', get_stylesheet_directory_uri() . '/assets/admin/options/watermark/watermark.css' );
      }
    }

    /**
     * Replace uw-2014 scripts.
     *
     * Hooked to the wp_print_scripts action, with a late priority (100),
     * so that it is after the script was enqueued.
     * Note. The add_action for this function is disabled as the contact card issue has been resolved in the parent theme.
     */
    static function uwdgh_replace_script() {
        //replace UW's contact-card widget script with local script
        wp_dequeue_script('contact-card');
        wp_register_script('contact-card-fix', get_stylesheet_directory_uri() . '/assets/admin/js/widgets/uw.contact-widget.js', array('jquery'));
        wp_enqueue_script('contact-card-fix');
    }
    //add_action( 'wp_print_scripts', 'uwdgh_replace_script', 100 );

    /**
     * Add the parent theme style.css.
     * This method is preferred over importing it into the style sheet
     */
    static function uwdgh_scripts_and_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

        wp_register_script('uwdgh', get_stylesheet_directory_uri() . '/js/uwdgh.js', array('jquery'));
        wp_enqueue_script('uwdgh');
    }

    /* theme headers */
    static function uwdgh_default_headers() {
      return array(
          'dgh' => array(
              'url'           => '%2$s/assets/header/Global_Health_2014.jpg',
              'thumbnail_url' => '%2$s/assets/header/Global_Health_2014-thumbnail.jpg',
              'description'   => 'DGH Map'
          ),
          'w3' => array(
              'url'           => '%2$s/assets/header/w3.jpg',
              'thumbnail_url' => '%2$s/assets/header/w3-thumbnail.jpg',
              'description'   => 'W Statue'
          ),
          'w1' => array(
              'url'           => '%2$s/assets/header/w1.jpg',
              'thumbnail_url' => '%2$s/assets/header/w1-thumbnail.jpg',
              'description'   => 'W Fingers'
          ),
          'blossoms' => array(
              'url'           => '%2$s/assets/header/blossoms.jpg',
              'thumbnail_url' => '%2$s/assets/header/blossoms-thumbnail.jpg',
              'description'   => 'Blossoms'
          ),
          'fall-leaves' => array(
              'url'           => '%2$s/assets/header/fall-leaves.jpg',
              'thumbnail_url' => '%2$s/assets/header/fall-leaves-thumbnail.jpg',
              'description'   => 'Fall Leaves'
          ),
          'ima' => array(
              'url'           => '%2$s/assets/header/ima.jpg',
              'thumbnail_url' => '%2$s/assets/header/ima-thumbnail.jpg',
              'description'   => 'IMA'
          ),
          'phf' => array(
              'url'           => '%2$s/assets/header/10_grant-lane-looking-west-small.jpg',
              'thumbnail_url' => '%2$s/assets/header/10_grant-lane-looking-west-small-thumbnail.jpg',
              'description'   => 'PHF'
          ),
      );
    }

    /* register thinstrip menu location */
    static function uwdgh_register_thinstrip_menu() {
        register_nav_menu('thinstrip-menu',__( 'Thinstrip','uwdgh' ));
    }

    /* Builds the default thinstrip menu for DGH.
     * This menu is persistant, meaning if deleted via the admin interface
     * it will be recreated.
     *
     * Admins can override by creating a new menu and assigning it to the
     * Thinstrip menu location.
     */
    public static function uwdgh_thinstrip_default_menu() {
        // Check if the menu exists
        $menu_name = "Global Health Audience Menu";
        $menu_exists = wp_get_nav_menu_object( $menu_name );
        // If it doesn't exist, let's create it.
        if( !$menu_exists){
            // Create the menu
            $menu_id = wp_create_nav_menu($menu_name);
            // Set up default menu items
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' =>  __('Support Us', 'uwdgh'),
                'menu-item-url' => 'https://globalhealth.washington.edu/support-us',
                'menu-item-attr-title' => __( 'Donate to Global Health', 'uwdgh' ),
                'menu-item-status' => 'publish'));
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' =>  __('Contact Us', 'uwdgh'),
                'menu-item-url' => 'https://globalhealth.washington.edu/contact',
                'menu-item-attr-title' => __('Contact Us', 'uwdgh'),
                'menu-item-status' => 'publish'));

            // add the menu to the location just once
            $run_once = get_option('menu_check');
            if (!$run_once){
                //then get the menu object by its name
                $menu = get_term_by( 'name', $menu_name, 'nav_menu' );
                // set menu to the thinstrip location
                $locations = get_theme_mod('nav_menu_locations');
                $locations['thinstrip-menu'] = $menu->term_id;
                set_theme_mod( 'nav_menu_locations', $locations );
                // update the menu_check option to make sure this code only runs once
                update_option('menu_check', true);
            }
        }
    }

    /**
    * Login/Logout link
    */
    public static function uwdgh_site_login_link() {
      if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        echo '<a href="' . wp_logout_url(get_permalink()) . '" title="' . __('Log out', 'uwdgh') . '">' . __('Log out', 'uwdgh') . ' <em>' . $current_user->user_login . '</em></a>';
      } else {
        echo '<a href="' . wp_login_url(get_permalink()) . '" title="' . __('Log in', 'uwdgh') . '">' . __('Log in', 'uwdgh') . '</a>';
      }
    }

  }

  UWDGH::init();
}
