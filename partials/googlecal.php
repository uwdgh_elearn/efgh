<style>
.responsiveGoogleCal {
	position: relative;
	padding-bottom: 75%;
	height: 0;
	overflow: hidden; }
.responsiveGoogleCal iframe {
	position: absolute;
	top: -40px;
	left: 0;
	width: 100%;
	height: 100%;
}
</style>
<div class="responsiveGoogleCal">
  <iframe src="https://calendar.google.com/calendar/u/0/embed?color=%239fe1e7&amp;src=efgh@uw.edu&amp;pli=1" frameborder="0" scrolling="no"></iframe>
</div>
