<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>
<?php
$template = "dlm-tablerow";  // template file: download-monitor/content-download-dlm-tablerow.php

// set table opening structure for loop_start
$table = "<div class='table-responsive'><table id='dlm-table-".bin2hex(random_bytes(4))."' class='table table-hover table-bottom_bordered table-condensed table-clear-default table--dlm'>";
$thead = "<thead><tr>";
$thead .= "<th></th>";
$thead .= "<th>Title</th>";
$thead .= "<th>filename</th>";
$thead .= "<th>date</th>";
$thead .= "<th>size</th>";
$thead .= "<th></th>";
$thead .= "</tr></thead>";

// set wrapper attributes
$loop_start = $table . $thead . "<tbody>";
$loop_end = "</tbody></table></div>";
$before = ""; // provide empty string to override default '<li>' value
$after = ""; // provide empty string to override default '</li>' value

// construct [downloads] shortcode
$shortcode = '[downloads';
$shortcode .= ' ' . $atts['taxonomy'] . '="' . $atts['tax_term'] . '"';
$shortcode .=	' template="' . $template . '"';
$shortcode .=	' loop_start="' . $loop_start . '"';
$shortcode .=	' loop_end="' . $loop_end . '"';
$shortcode .=	' before="' . $before . '"';
$shortcode .=	' after="' . $after . '"';
// optionals
if ($atts['id']) {
  $shortcode .=	' include="' . $atts['id'] . '"';
}
if ($atts['order']) {
  $shortcode .=	' order="' . $atts['order'] . '"';
}
if ($atts['orderby']) {
  $shortcode .=	' orderby="' . $atts['orderby'] . '"';
}
$shortcode .=	']';

?>
<?php echo do_shortcode( $shortcode ); ?>
